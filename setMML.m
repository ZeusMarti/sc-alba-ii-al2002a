%% set MatLab Middle Layer
% oblanco
% 2023oct16


if isempty(getenv('MMLROOT'))
    fprintf('This session does not define MML\n');
else
    mmlroot = getenv('MMLROOT');
end
addpath(fullfile(mmlroot));
addpath(fullfile(mmlroot,'mml'));

