function [SC,dBx,dBy] = performLOCO_albaII(SC,BPMords,CMords,varargin)
% performLOCO_ALSU_SR
% ===================
%
% NAME
% ----
% performLOCO_ALSU_SR - Performs LOCO for the ALSU-SR
%
% SYNOPSIS
% --------
% `SC = performLOCO_ALSU_SR(SC,BPMords,CMords [,options])`
%
% DESCRIPTION
% -----------
% This function performs LOCO for the ALSU-SR lattice.
%
% INPUT
% -----
% `SC`::
% 	The SC base structure
% `BPMords`::
% 	BPM ordinates used for orbit correction.
% `CMords`::
% 	CM ordinates used for orbit correction
%
% OPTIONS
% -------
% The following options can be given as name/value-pairs:
%
% `'nIter'` (1:8)`)::
%	Iterations to be carried out.
% `'CMstep'` (50E-6)::
% 	CM steps for RM measurement.
% `'deltaRF'` (1500)::
% 	RF step for dispersion measurement.
% `'RMpar'` ({})::
% 	Parameters for RM measurement.
% `'HorizontalDispersionWeight'` (1)::
%	Horizontal dispersion weight for LOCO.
% `'VerticalDispersionWeight'` (1)::
%	Vertical dispersion weight for LOCO.
% `'measRMeveryStep'` (0)::
%	Specify if RM should be re-measured after each correction step.
% `'RMstruct'` ([])::
%	Structure containing all information relevant for orbit correction.
%
% RETURN VALUE
% ------------
% `SC`::
% 	The SC base structure with corrected lattice.
%
% SEE ALSO
% --------
% *performDiagCorr_ALSU_SR*, *performOrbitCorr_ALSU_SR*


% Parse optional arguments
p = inputParser;
addOptional(p,'nIter',1:8);
addOptional(p,'maxLocoIt',3);
addOptional(p,'RMpar',{});
addOptional(p,'diagCorr',1);
addOptional(p,'CMstep',100E-6); % CM step for response matrix calculation
addOptional(p,'deltaRF',500); % RF step for dispersion calculation
addOptional(p,'RMfolder','/home/zeus/ZeusApps/Studies/SC_albaII/LOCO/'); %
addOptional(p,'RMstruct',[]); % RM used for orbit correction (inkl. dispersion)
addOptional(p,'HorizontalDispersionWeight',10); % Hor. dispersion wight in LOCO (not used in diag-correction)
addOptional(p,'VerticalDispersionWeight',10); % Ver. dispersion wight in LOCO (not used in diag-correction)
parse(p,varargin{:});
par = p.Results;

% For use of precalculated LOCO RMs
metaData.RMfolder   = par.RMfolder;
metaData.usePreCalc = 0;

% Load CM calibration offsets
%load('210103_SR_v21_CM_cal_Offsets');


% Set up BPM and CM data structure structures
[BPMData,CMData] = SClocoLib('getBPMCMstructure',SC,par.CMstep,...
        {'BPM','FitGains','No'},...
        {'CM','FitKicks','No'},...
        {'CMords',CMords{1},CMords{2}});

% Setup LOCO model
[LOCOmodel,LocoFlags,Init] = SClocoLib('setupLOCOmodel',SC,...
    'HorizontalDispersionWeight',par.HorizontalDispersionWeight,...
    'VerticalDispersionWeight'  ,par.VerticalDispersionWeight,...
    'SVmethod',1E-2);



% Initialize correction progress structure (just for plotting)
CorrProgress.RING{1} = SC.RING;
CorrProgress.PAR{1}  = [];


% Calculate model response matrix
if isempty(par.RMstruct)
    par.RMstruct.RM         = SCgetModelRM(SC,BPMords,CMords,'trackMode','ORB','useIdealRing',1);
    par.RMstruct.eta        = SCgetModelDispersion(SC,BPMords,SC.ORD.Cavity);
    par.RMstruct.scaleDisp  = 1E7;
    par.RMstruct.BPMords    = BPMords;
    par.RMstruct.CMords     = CMords;
    par.RMstruct.alpha      = 5;
    par.RMstruct.CMsteps{1} = 50E-6 ./ mean(abs(RMstruct.RM(1:length(BPMords),1:length(CMords{1}))));
    par.RMstruct.CMsteps{2} = 50E-6 ./ mean(abs(RMstruct.RM(length(BPMords)+1:end,length(CMords{1})+1:end)));
end


%% Run LOCO
LocoFlags.CalculationType='fast'; %has no effect if function loco instead of loco_fast
LinData0 = atlinopt(SC.IDEALRING,0,[BPMords numel(SC.RING)]);
b0=reshape([LinData0.beta],[2,numel(BPMords)+1])';
phi0=reshape([LinData0.mu],[2,numel(BPMords)+1])'/2/pi;
Bx0=b0(1:(end-1),1);
By0=b0(1:(end-1),2);
px0=phi0(1:(end-1),1);
py0=phi0(1:(end-1),2);

for nIter=par.nIter
    % Get orbit response matrix and dispersion from measurement
    LocoMeasData = SClocoLib('getMeasurement',SC,par.CMstep,par.deltaRF,BPMords,CMords,par.RMpar{:});

   % here the RM calculation is subtituded by the LOCO used method
    RINGData.Lattice=SC.RING;
    RINGData.CavityFrequency=SC.RING{SC.ORD.Cavity}.Frequency;
    RINGData.CavityHarmNumber=SC.RING{SC.ORD.Cavity}.HarmNumber;
    RM = locoresponsematrix(RINGData, BPMData, CMData);
    LocoMeasData.M=RM;
    
    
    LinData = atlinopt(SC.RING,0,[BPMords numel(SC.RING)]);
    b=reshape([LinData.beta],[2,numel(BPMords)+1])';
    phi=reshape([LinData.mu],[2,numel(BPMords)+1])'/2/pi;
    Bx=b(1:(end-1),1);
    By=b(1:(end-1),2);
    px=phi(1:(end-1),1);
    py=phi(1:(end-1),2);
    dBx(nIter)=std((Bx-Bx0)./Bx0);
    dBy(nIter)=std((By-By0)./By0);
    dpx(nIter)=std(px-px0);
    dpy(nIter)=std(py-py0);
    
    
    if nIter==1
        FitParameters = SClocoLib('setupFitparameters',SC,Init.SC.RING,LOCOmodel,par.deltaRF,...
            {SCgetOrds(SC.RING,'^Q1$'),'normal','individual',5E-3},... % {Ords, normal/skew, ind/fam, deltaK}
            {SCgetOrds(SC.RING,'^Q2$'),'normal','individual',5E-3},...
            {SCgetOrds(SC.RING,'^Q3$'),'normal','individual',5E-3},...
            {SCgetOrds(SC.RING,'^IQ1$'),'normal','individual',5E-3},...
            {SCgetOrds(SC.RING,'^IQ2$'),'normal','individual',5E-3},...
            {SCgetOrds(SC.RING,'^IQ3$'),'normal','individual',5E-3},...
            {SCgetOrds(SC.RING,'^QD$'),'normal','individual',5E-3},...
            {SCgetOrds(SC.RING,'^QF$'),'normal','individual',5E-3},...
            {SCgetOrds(SC.RING,'^QFS$'),'normal','individual',5E-3});
        
    elseif nIter==3
        % Set up LOCO fit parameter structure
        
        FitParameters = SClocoLib('setupFitparameters',SC,Init.SC.RING,LOCOmodel,par.deltaRF,...
            {SCgetOrds(SC.RING,'^Q1$'),'normal','individual',1E-3},... % {Ords, normal/skew, ind/fam, deltaK}
            {SCgetOrds(SC.RING,'^Q2$'),'normal','individual',1E-3},...
            {SCgetOrds(SC.RING,'^Q3$'),'normal','individual',1E-3},...
            {SCgetOrds(SC.RING,'^IQ1$'),'normal','individual',1E-3},...
            {SCgetOrds(SC.RING,'^IQ2$'),'normal','individual',1E-3},...
            {SCgetOrds(SC.RING,'^IQ3$'),'normal','individual',1E-3},...
            {SCgetOrds(SC.RING,'^QD$'),'normal','individual',1E-3},...
            {SCgetOrds(SC.RING,'^QF$'),'normal','individual',1E-3},...
            {SCgetOrds(SC.RING,'^QFS$'),'normal','individual',1E-3},...
            {SC.ORD.SkewQuad,'skew','individual',5E-3});
        
        LocoFlags.Coupling   = 'Yes'; % Include off-diagonal ORM elements
        LocoFlags.Dispersion = 'Yes'; % Include dispersion
        
    end
    
    
    % Run LOCO
    for locoit=1:par.maxLocoIt
        [~,BPMData, CMData, FitParameters, LocoFlags, LOCOmodel] = loco_fast(LocoMeasData,  BPMData,  CMData,  FitParameters,  LocoFlags,  LOCOmodel);
    end
    
    % Apply lattice correction step
    SC = SClocoLib('applyLatticeCorrection',SC,FitParameters,'damping',0.3,'dipCompensation',1);
    
    % Apply orbit correction
    SC = performOrbitCorr_albaII(SC,par.RMstruct,'weight',[]);
    
    % Update progress structure
    CorrProgress.RING{end+1} = SC.RING;
    CorrProgress.PAR{end+1} = FitParameters;
    
    % Plot progress
    SClocoLib('plotStatus',SC,Init,BPMData,CMData) ;
    
%     
%     if nIter > 3
%         % Match chromaticity
%         SC = SClocoLib('fitChromaticity',SC,SCgetOrds(SC.RING,{'SF','SD'}),'targetChrom',[2 1],'verbose',1);
%         
%         % Apply orbit correction
%         [SC,~] = performOrbitCorr_ALSU_SR(SC,par.RMstruct);
%         
%         % Update progress structure
%         CorrProgress.RING{end+1} = SC.RING;
%         
%     end
end

% 	plotProgress(SC,CorrProgress,Init)

end





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot correction progress %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plotProgress(SC,CorrProgress,Dist)

OUT = calcLatticeProperties_ALSU_SR(Dist.SC);
Dist.betaBeat = OUT.betaBeat;

nSteps = length(CorrProgress.RING);
nPlots = 5;
tmp=[];
for nStep=1:nSteps
    tmpSC = SC;
    tmpSC.RING = CorrProgress.RING{nStep};
    OUT = calcLatticeProperties_ALSU_SR(tmpSC);
    tmp.betaBeat(nStep,:)     = OUT.betaBeat;
    tmp.dispBeat(nStep,:)     = OUT.dispBeat;
    tmp.tuneShift(nStep,:)    = OUT.tuneShift;
    tmp.Chromaticity(nStep,:) = OUT.Chromaticity;
    tmp.Coupling(nStep,:)     = OUT.Coupling;
    tmp.Emittance(nStep,:)    = OUT.Emittance;
    tmp.orbit(nStep,:)        = OUT.orbit;
end
% Fit parameters
par=[];ords=[];
for nStep=2:length(CorrProgress.PAR)
    for nPar=1:length(CorrProgress.PAR{nStep}.Params)
        ord = CorrProgress.PAR{nStep}.Params{nPar}.ElemIndex;
        par(nStep,ord) = CorrProgress.PAR{nStep}.Values(nPar)/CorrProgress.PAR{nStep}.OrigValues(nPar);
    end
end
par(par==0)=nan;
figure(4467)
plot(par)

titleBBstr  = {'hor. \beta-beat','ver. \beta-function'};titleDBstr = {'hor. \Delta\eta','ver. \Delta\eta'};

figure(446),clf
for nDim=1:2
    subplot(2,nPlots,nPlots*(nDim-1)+1)
    plot(1E2*repmat(Dist.betaBeat(nDim),nSteps,1),'k','LineWidth',2);hold on
    plot(1E2*tmp.betaBeat(:,nDim),'O--','LineWidth',2);
    xlabel('Iteration');ylabel('\Delta\beta/\beta_0 [%]');title(titleBBstr{nDim})
    set(gca,'YScale','log')%,'YLim',yLim)
    
    subplot(2,nPlots,nPlots*(nDim-1)+2)
    plot(1E3*repmat(Dist.dispBeat(nDim),nSteps,1),'k','LineWidth',2);hold on
    plot(1E3*tmp.dispBeat(:,nDim),'O--','LineWidth',2);
    xlabel('Iteration');ylabel('\Delta\eta [mm]');title(titleDBstr{nDim})
    
    subplot(2,nPlots,nPlots*(nDim-1)+3)
    plot(abs(repmat(Dist.tuneShift(nDim),nSteps,1)),'k','LineWidth',2);hold on
    plot(abs(tmp.tuneShift(:,nDim)),'O--','LineWidth',2);
    xlabel('Iteration');ylabel('\Delta\nu [mm]');title('Tune')
    set(gca,'YScale','log')
    
    subplot(2,nPlots,nPlots*(nDim-1)+4)
    plot(repmat(Dist.Chromaticity(nDim),nSteps,1),'k','LineWidth',2);hold on
    plot(tmp.Chromaticity(:,nDim),'O--','LineWidth',2);
    xlabel('Iteration');ylabel('\xi');title('Chromaticity')
    
    subplot(2,nPlots,nPlots*(nDim-1)+5)
    plot(repmat(Dist.Coupling,1,nSteps)','k','LineWidth',2);hold on
    plot(tmp.Coupling,'O--','LineWidth',2);
    xlabel('Iteration');ylabel('RM');title('Coupling')
    set(gca,'YScale','log')
end
set(findall(gcf,'-property','FontSize'),'FontSize',20);set(gcf,'color','w');drawnow

end
