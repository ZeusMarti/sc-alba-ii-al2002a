function run_albaII_sc_HCMinSH(inputdatafile)
t_initial=now;
% === Setup enviroment

% Clear workspace, initialize the global variable `plotFunctionFlag`.
global plotFunctionFlag THERING;
warning off;

%% set path to code
if not(isdeployed())
    setAT;
    setSC;
    setMML;
end


dobbaco=true;
dobbatbt=false;

a=load(inputdatafile);
outputdatafile=a.outfilename;
rf_correction_method=a.rf_correction_method;
var=a.var;
%dipCompensation=var(3);
skrange=a.skrange;
BPMoffsetRMS=a.BPMoffsetRMS;
seed=var(1);

rng(seed);% chose random generator seed

%devrand=fopen('/dev/urandom','r'); rngseed= fread(devrand,1,'*uint32'); rng(rngseed); fclose(devrand);



sba_ssh;
RING=THERING;
ati=atindex(RING);
RING{ati.CAVRF}.TimeLag = 0.0272;
RING = SCcronoff(RING,'cavityon','radiationon');

% Initialize the SC toolbox with the previously defined lattice cell structure.
SC = SCinit(RING);

% === Register lattice in SC
[SC, BPMords, CMords,skords] = register_albaII_1PS_HCMinSH(SC,BPMoffsetRMS,skrange);


% === Define lattice apertures

% In this section a simple aperture model is defined. The aperture radius of
% all drift spaces is 13mm, while an circular aperture is implemented in all
% magnets with a radius of 10mm. In order to create a `pinhole`, the 50th
% magnet is randomly choosen to get a small eliptical aperture.
for ord=SCgetOrds(SC.RING,'^D7CM$|^D9CM$|^SS$')
    SC.RING{ord}.EApertures = 8E-3 * [1 1]; % [m]
end

for ord=SCgetOrds(SC.RING,'^QD$|^QF$|^QFS$|^QDS$|^Q1$|^Q2$|^Q3$|^SH\d$|^SV[1,2,3,4,5,6,7,8,9]$')
    SC.RING{ord}.EApertures = 8E-3 * [1 1]; % [m]
end

%SC.RING{SC.ORD.Magnet(50)}.EApertures = [6E-3 3E-3]; % [m]

% === Check registration

% In this section the SC registration is checked for consistency and the
% lattice is plotted.
SCsanityCheck(SC);

% SCplotLattice(SC,'nSectors',16);

% === Apply errors

% The next step is to generate and apply an error set based on the previolusly
% defined uncertainties. The misalignments are plotted.
SC = SCapplyErrors(SC);


% SCplotSupport(SC);

% === Setup correction chain

% At this point the parameters of the correction chain may be defined. In this
% example, we switch off the cavity and the sextupole magnets. Furthermore the
% 1 and 2-turn model trajectory response matrices are calcualted and a Tikhonov
% regularization with a regularization parameter of `50` is used to calculate
% the pseudo inverse of both matrices..
SC.RING = SCcronoff(SC.RING,'cavityoff','radiationon');

sextOrds = SCgetOrds(SC.RING,'^SH\d$|^SV[1,2,3,4,5,6,7,8,9]$');
SC = SCsetMags2SetPoints(SC,sextOrds,2,3,0,'method','abs');

RM1 = SCgetModelRM(SC,BPMords,CMords,'nTurns',1);
RM2 = SCgetModelRM(SC,BPMords,CMords,'nTurns',2);

Minv1 = SCgetPinv(RM1,'alpha',50);
Minv2 = SCgetPinv(RM2,'alpha',50);

%
% Next, we define the number of particles per bunch, shots for averaging the
% BPM reading and number of turns and ensure turn-by-turn tracking mode. The
% noise level `eps` defines a stopping criteria for the feedback. Finally, we
% switch on the global plot falg and plot uncorrected beam trajectory.
SC.INJ.nParticles = 1;
SC.INJ.nTurns     = 1;
SC.INJ.nShots     = 1;
SC.INJ.trackMode  = 'TBT';

eps   = 1E-4; % Noise level

plotFunctionFlag = 0;

SCgetBPMreading(SC);

% === Start correction chain

% Run first turn feedback and apply correction if no error occured.
[CUR,ERROR] = SCfeedbackFirstTurn(SC,Minv1,'BPMords',BPMords,'CMords',CMords,'verbose',1);


if ~ERROR; SC=CUR;
else
    t_final=now;
    elapsed_time=(t_final-t_initial)*24*3600;
    save_nan_before_exit(outputdatafile,elapsed_time);
    return;
end
Td=zeros(10,100);
T=ringpass(SC.RING,[1e-6 0 0 0 0 0]',100);Td(1,:)=T(5,:);
% Switch in 2-turn mode and get full 2-turn transmission by correcting the
% first three BPMs of the second turn to the corresponding readings in the
% first turn.
SC.INJ.nTurns = 2;

[CUR,ERROR] = SCfeedbackStitch(SC,Minv2,...
    'BPMords',BPMords,...
    'CMords',CMords,...
    'nBPMs',3,...
    'maxsteps',50,...
    'verbose',1);
if ~ERROR; SC=CUR;
else
    t_final=now;
    elapsed_time=(t_final-t_initial)*24*3600;
    save_nan_before_exit(outputdatafile,elapsed_time);
    return;
end

T=ringpass(SC.RING,[1e-6 0 0 0 0 0]',100);Td(2,:)=T(5,:);

% Run trajectory feedback on 2-turn readings.
[CUR,ERROR] = SCfeedbackRun(SC,Minv2,...
    'BPMords',BPMords,...
    'CMords',CMords,...
    'target',300E-6,...
    'maxsteps',30,...
    'eps',eps,...
    'verbose',1);
if ~ERROR; SC=CUR;
else
    t_final=now;
    elapsed_time=(t_final-t_initial)*24*3600;
    save_nan_before_exit(outputdatafile,elapsed_time);
    return;
end

T=ringpass(SC.RING,[1e-6 0 0 0 0 0]',100);Td(3,:)=T(5,:);

%% Quads TbT BBA
straight_status=repmat([true true false(1,5) true true],[1 16]);
quadOrds = repmat(SCgetOrds(SC.RING,'^Q1$|^Q3$'),[2 1]);
BPMordsQuadBBA=repmat(SC.ORD.BPM(straight_status),2,1);
magSPvec = repmat({linspace(0.95,1.05,2)},size(BPMordsQuadBBA));
inds=1:size(quadOrds,2);
if dobbatbt
    [SC,errorFlags] = SCBBA(SC,BPMordsQuadBBA(:,inds),quadOrds(:,inds),...
        'mode','TBT',...
        'fakeMeasForFailures',1,...
        'outlierRejectionAt',Inf,...
        'quadOrdPhaseAdvance',quadOrds(1),...
        'quadStrengthPhaseAdvance',[0.95 1.05],...
        'magSPvec',magSPvec,...
        'nSteps',10,...
        'verbose',1,...
        'plotResults',0,...
        'plotLines',0,...
        'minSlopeForFit',0.03,...
        'minBPMrangeAtBBABBPM',200E-6,...
        'BBABPMtarget',1E-3,...
        'minBPMrangeOtherBPM',100E-6,...
        'maxStdForFittedCenters',600E-6);
end
%% Tbt BBA sextupoles

stat_arc_bpms=logical(repmat([0; 0; ones(5,1); 0; 0],[16,1]));
BPMordsSextBBA=SC.ORD.BPM(stat_arc_bpms);
magSPvecSext = repmat(repmat({linspace(-skrange,skrange,2)},size(BPMordsSextBBA)),[2 1]);

LinData_BPM = atlinopt(SC.IDEALRING,0,BPMordsSextBBA);
phi_BPM=reshape([LinData_BPM.mu],[2,numel(BPMordsSextBBA)])'/2/pi;

LinData_sk = atlinopt(SC.IDEALRING,0,SC.ORD.SkewQuad);
phi_sk=reshape([LinData_sk.mu],[2,numel(SC.ORD.SkewQuad)])'/2/pi;

sextOrdsBBA=zeros(2,numel(BPMordsSextBBA));

[~,closestsext]=min(abs(phi_BPM(:,2)-phi_sk(:,2)'),[],2);
sextOrdsBBA(1,:)=SC.ORD.SkewQuad(closestsext);

[~,closestsext]=min(abs(phi_BPM(:,1)-phi_sk(:,1)'),[],2);
sextOrdsBBA(2,:)=SC.ORD.SkewQuad(closestsext);

inds=1:size(BPMordsSextBBA,2);
if dobbatbt
    SC = SCBBA(SC,repmat(BPMordsSextBBA(inds),[2 1]),sextOrdsBBA(:,inds),...
        'mode','TBT',...
        'fakeMeasForFailures',1,...
        'outlierRejectionAt',Inf,...
        'quadOrdPhaseAdvance',quadOrds(1),...
        'quadStrengthPhaseAdvance',[0.95 1.05],...
        'plotLines',0,...
        'plotResults',0,...
        'verbose',1,...
        'nSteps',10,...
        'switchOffSext',0,...
        'magSPflag','abs',...
        'magSPvec',magSPvecSext,...
        'minSlopeForFit',0.01,...
        'minBPMrangeAtBBABBPM',1E-6,...
        'BBABPMtarget',1.0E-3,...
        'minBPMrangeOtherBPM',200e-6,...
        'maxStdForFittedCenters',1000E-6,...
        'skewQuadrupole',1);
end


%% Run trajectory feedback on 2-turn readings. Then create a period 1 orbit by matching
% the second turn BPM readings to the first turn

if dobbatbt
    [CUR,ERROR] = SCfeedbackRun(SC,Minv2,...
        'BPMords',BPMords,...
        'CMords',CMords,...
        'target',100E-6,...
        'maxsteps',30,...
        'eps',eps,...
        'verbose',1);
    if ~ERROR; SC=CUR;
    else
        t_final=now;
        elapsed_time=(t_final-t_initial)*24*3600;
        save_nan_before_exit(outputdatafile,elapsed_time);
        return;
    end
    
    [CUR,ERROR] = SCfeedbackBalance(SC,Minv2,...
        'BPMords',BPMords,...
        'CMords',CMords,...
        'maxsteps',32,...
        'eps',eps,...
        'verbose',1);
    if ~ERROR
        SC=CUR;
    else
        t_final=now;
        elapsed_time=(t_final-t_initial)*24*3600;
        save_nan_before_exit(outputdatafile,elapsed_time);
        return;
    end
    
end

plotFunctionFlag = 0;
T=ringpass(SC.RING,[1e-6 0 0 0 0 0]',100);Td(4,:)=T(5,:);
% Use drift space between last BPM of first turn and 1st BPM of second turn to fit the
% injection trajectory
[deltaZ0,ERROR] = SCfitInjectionZ(SC,'injectionDrift','verbose',1);
T=ringpass(SC.RING,[1e-6 0 0 0 0 0]',100);Td(5,:)=T(5,:);
if ~ERROR
    initialZ0 = SC.INJ.Z0;
    for nStep=1:5
        SC.INJ.Z0 = initialZ0 + deltaZ0 * nStep/5;
        [CUR,ERROR] = SCfeedbackRun(SC,Minv2,...
            'BPMords',BPMords,...
            'CMords',CMords,...
            'target',100E-6,...
            'maxsteps',30,...
            'eps',eps,...
            'verbose',1);
        if ~ERROR; SC=CUR; else; break; end
    end
else
    t_final=now;
    elapsed_time=(t_final-t_initial)*24*3600;
    save_nan_before_exit(outputdatafile,elapsed_time);
    return;
end



%
% In the following loop the sextupole magnets are ramped up in 5 steps
% and feedback is applied after each step.
T=ringpass(SC.RING,[1e-6 0 0 0 0 0]',100);Td(6,:)=T(5,:);

for S = linspace(0.1,1,5)
    
    SC = SCsetMags2SetPoints(SC,sextOrds,2,3,S,'method','rel');
    
    [CUR,ERROR] = SCfeedbackBalance(SC,Minv2,...
        'BPMords',BPMords,...
        'CMords',CMords,...
        'maxsteps',10,...
        'eps',eps,...
        'verbose',1);
    
    if ~ERROR
        SC=CUR;
    else
        t_final=now;
        elapsed_time=(t_final-t_initial)*24*3600;
        save_nan_before_exit(outputdatafile,elapsed_time);
        return;
    end
end



% Switch off plotting every beam, switch the cavity on and plot initial
% phase space.
plotFunctionFlag = 0;

SC.RING = SCcronoff(SC.RING,'cavityon');

%SCplotPhaseSpace(SC,'nParticles',10,'nTurns',100);


% The following block performs an rf phase and frequency correction in
% a loop and applies the corresponding correction step if no error
% occured.

plotProgress=0;
plotResults=0;
fMax = 50E3;
f0 = SC.RING{SC.ORD.Cavity}.Frequency;
nturnss=[60 90 100 120 150];
fRanges=[20E3 10E3 5E3 2E3 1E3];

for nIter=1:5
    
    
    fRange=fRanges(nIter)*[-1 1];
    nturns=nturnss(nIter);
    % Perform RF frequency correction.
    if strcmpi(rf_correction_method,'zeus')
        [deltaF,ERROR] = SCsynchEnergyCorrection_zeus(SC,...
            'range',fRange,... % Frequency range [kHz]
            'nTurns',nturns,...         % Number of turns
            'nSteps',45,...         % Number of frequency steps
            'minTurns',20,...       % Minimum number of turns required for measurment
            'plotResults',plotResults,...     % Final results are plotted
            'plotProgress',plotProgress,...     % Final results are plotted
            'verbose',1);           % Print results
    elseif strcmpi(rf_correction_method,'default')
        [deltaF,ERROR] = SCsynchEnergyCorrection(SC,...
            'range',fRange,... % Frequency range [kHz]
            'nTurns',nturns,...         % Number of turns
            'nSteps',30,...         % Number of frequency steps
            'minTurns',20,...       % Minimum number of turns required for measurment
            'plotResults',plotResults,...     % Final results are plotted
            'plotProgress',plotProgress,...     % Final results are plotted
            'verbose',1);           % Print results
    else
        error('wrong rf method.');
    end
    % Apply frequency correction
    if ~ERROR
        if deltaF~=min(max(deltaF,-fMax),fMax)
            fprintf('Energy correction %.0fkHz above threshold and will be cliped.\n',fMax)
            deltaF = min(max(deltaF,-fMax),fMax);
        end
        % Apply frequency correction
        SC = SCsetCavs2SetPoints(SC,SC.ORD.Cavity,'Frequency',deltaF,'add');
        
        
    else
        t_final=now;
        elapsed_time=(t_final-t_initial)*24*3600;
        save_nan_before_exit(outputdatafile,elapsed_time);
        return;
    end
    
    % Perform RF phase correction.
    [deltaPhi,ERROR] = SCsynchPhaseCorrection(SC,...
        'nTurns',30,...      % Number of turns
        'nSteps',35,...     % Number of phase steps
        'plotResults',plotResults,... % Final results are plotted
        'plotProgress',plotProgress,...     % Final results are plotted
        'verbose',1);       % Print results
    if ERROR
        warning('Phase correction crashed');
        t_final=now;
        elapsed_time=(t_final-t_initial)*24*3600;
        save_nan_before_exit(outputdatafile,elapsed_time);
        return;
    end
    
    % Apply phase correction
    SC = SCsetCavs2SetPoints(SC,SC.ORD.Cavity,...
        'TimeLag',deltaPhi,...
        'add');
    

    T=ringpass(SC.RING,[1e-6 0 0 0 0 0]',100);Td(6+nIter,:)=T(5,:);
end
f1 = SC.RING{SC.ORD.Cavity}.Frequency;

plot(Td');

% Plot final phase space and check if beam capture is achieved.
%SCplotPhaseSpace(SC,'nParticles',10,'nTurns',1000);

[maxTurns,lostCount,ERROR] = SCgetBeamTransmission(SC,...
    'nParticles',10,...
    'nTurns',100,...
    'verbose',true);
if ERROR
    t_final=now;
    elapsed_time=(t_final-t_initial)*24*3600;
    save_nan_before_exit(outputdatafile,elapsed_time);
    return;
end

fprintf('Total frequency change: %.2f kHz',1E-3*(f0-f1));
isorbit=not(any(isnan(findorbit6(SC.RING))));
fprintf('\nClosed Orbit:is %d (0 Nan, 1 OK)\n',isorbit);





%% There is orbit

if isorbit
    % First Orbit correction before BBA
    % Beam capture achieved, switch to orbit mode for tracking. Calculate the orbit
    % response matrix and the dispersion. Assume a beam based alignment procedure
    % reduces the BPM offsets to 50um rms w.r.t. their neighbouring QF/QD magnets.
    SC.INJ.trackMode = 'ORB';
    MCO = SCgetModelRM(SC,BPMords,CMords,'trackMode','ORB','useIdealRing',1);
    
    LinData = atlinopt(SC.RING,0,[BPMords numel(SC.RING)]);
    b=reshape([LinData.beta],[2,numel(BPMords)+1])';
    phi=reshape([LinData.mu],[2,numel(BPMords)+1])'/2/pi;
    Bx1=b(1:(end-1),1);
    By1=b(1:(end-1),2);
    px1=phi(1:(end-1),1);
    py1=phi(1:(end-1),2);
    
    LinData0 = atlinopt(SC.IDEALRING,0,[BPMords numel(SC.RING)]);
    b0=reshape([LinData0.beta],[2,numel(BPMords)+1])';
    phi0=reshape([LinData0.mu],[2,numel(BPMords)+1])'/2/pi;
    Bx0=b0(1:(end-1),1);
    By0=b0(1:(end-1),2);
    px0=phi0(1:(end-1),1);
    py0=phi0(1:(end-1),2);
    
    dBx1=std((Bx1-Bx0)./Bx0);
    dBy1=std((By1-By0)./By0);
    dpx1=std(px1-px0);
    dpy1=std(py1-py0);
    
    % Take for OFB relevant entries of response matrix
    RMstruct.RM         = MCO;
    RMstruct.eta        = SCgetModelDispersion(SC,BPMords,SC.ORD.Cavity,'rfStep',5);
    RMstruct.scaleDisp  = 1E7;
    RMstruct.BPMords    = BPMords;
    RMstruct.CMords     = CMords;
    % Pseudo-inverse for BBA orbit bump
    RMstruct.MinvCO = SCgetPinv([RMstruct.RM RMstruct.scaleDisp*RMstruct.eta],'alpha',5);
    % Regularization parameters for orbit feedback loops
    RMstruct.alphaVec = [10 5:-1:1 0.9:-0.1:0.1];
    eta = SCgetModelDispersion(SC,SC.ORD.BPM,SC.ORD.Cavity);
    SC = performOrbitCorr_albaII(SC,RMstruct,'weight',[]);
    
    % BBA quads
    inds=1:size(BPMordsQuadBBA,2);
    magSPvec = repmat({linspace(0.95,1.05,2)},size(BPMordsQuadBBA));
    if dobbaco
        SC = SCBBA_1CM(SC,BPMordsQuadBBA(:,inds),quadOrds(:,inds),...
            'mode','ORB',...
            'fakeMeasForFailures',1,...
            'outlierRejectionAt',Inf,...
            'dipCompensation',0,...
            'RMstruct',RMstruct,...
            'plotLines',0,...
            'plotResults',0,...
            'verbose',1,...
            'nSteps',10,...
            'magSPvec',magSPvec,...
            'minSlopeForFit',0.005,...
            'minBPMrangeAtBBABBPM',1*20E-6,...
            'BBABPMtarget',0.5E-3,...
            'maxStdForFittedCenters',0.6e-3,...
            'minBPMrangeOtherBPM',0);
        
        %sexts
        inds=1:size(BPMordsSextBBA,2);
        SC = SCBBA_1CM(SC,repmat(BPMordsSextBBA(inds),[2 1]),sextOrdsBBA(:,inds),...
            'mode','ORB',...
            'fakeMeasForFailures',1,...
            'outlierRejectionAt',Inf,...
            'RMstruct',RMstruct,...
            'plotLines',0,...
            'plotResults',0,...
            'verbose',1,...
            'nSteps',10,...
            'switchOffSext',0,...
            'magSPflag','abs',...
            'magSPvec',magSPvecSext,...
            'minSlopeForFit',0.005,...
            'minBPMrangeAtBBABBPM',10E-6,...
            'BBABPMtarget',1E-3,...
            'minBPMrangeOtherBPM',0.0,...
            'skewQuadrupole',1);
    end
    
    SC = performOrbitCorr_albaII(SC,RMstruct,'weight',[]);
    
    
    LinData = atlinopt(SC.RING,0,[BPMords numel(SC.RING)]);
    b=reshape([LinData.beta],[2,numel(BPMords)+1])';
    phi=reshape([LinData.mu],[2,numel(BPMords)+1])'/2/pi;
    Bx=b(1:(end-1),1);
    By=b(1:(end-1),2);
    px=phi(1:(end-1),1);
    py=phi(1:(end-1),2);
    
    dBx2=std((Bx-Bx0)./Bx0);
    dBy2=std((By-By0)./By0);
    dpx2=std(px-px0);
    dpy2=std(py-py0);
    
    
    
    
    % Run LOCO fit procedure in a loop and apply the lattice correction after each
    % LOCO step, followed by an orbit correction step. After three iterations,
    % include coupling (off-diagonal response matrix blocks) and the skew
    % quadrupole correctors as LOCO fitparameters.
    try
        [SC,dBxit,dByit] = performLOCO_albaII(SC,BPMords,CMords,'nIter',1:8,'maxLocoIt',4,...
            'CMstep',70E-6,'deltaRF',300,'HorizontalDispersionWeight',50,...
            'VerticalDispersionWeight',50,'RMstruct',RMstruct);
        dBxit
        dByit
    catch me
        disp(me.message)
        for n=1:numel(me.stack)
            fprintf('%s %d \n',me.stack(n).name,me.stack(n).line)
        end
        t_final=now;
        elapsed_time=(t_final-t_initial)*24*3600;
        save_nan_before_exit_butorbit(outputdatafile,elapsed_time,dBx2,dBy2,dpx2,dpy2,dBx1,dBy1,dpx1,dpy1);
        return;
    end
    
    LinData = atlinopt(SC.RING,0,[BPMords numel(SC.RING)]);
    b=reshape([LinData.beta],[2,numel(BPMords)+1])';
    phi=reshape([LinData.mu],[2,numel(BPMords)+1])'/2/pi;
    Bx=b(1:(end-1),1);
    By=b(1:(end-1),2);
    px=phi(1:(end-1),1);
    py=phi(1:(end-1),2);
    
    dBx=std((Bx-Bx0)./Bx0);
    dBy=std((By-By0)./By0);
    dpx=std(px-px0);
    dpy=std(py-py0);
    
else
    dBx=nan;
    dBy=nan;
    dpx=nan;
    dpy=nan;
    dBx2=nan;
    dBy2=nan;
    dpx2=nan;
    dpy2=nan;
    dBx1=nan;
    dBy1=nan;
    dpx1=nan;
    dpy1=nan;
end
t_final=now;
elapsed_time=(t_final-t_initial)*24*3600;
save(outputdatafile,'dBx','dBy','dpx','dpy','dBx2','dBy2','dpx2','dpy2',...
    'dBx1','dBy1','dpx1','dpy1','isorbit','elapsed_time','dBxit','dByit');

end

function save_nan_before_exit_butorbit(outputdatafile,elapsed_time,dBx2,dBy2,dpx2,dpy2,dBx1,dBy1,dpx1,dpy1)
dBx=nan;
dBy=nan;
dpx=nan;
dpy=nan;


isorbit=nan;
save(outputdatafile,'dBx','dBy','dpx','dpy','dBx2','dBy2','dpx2','dpy2','dBx1','dBy1','dpx1','dpy1','isorbit','elapsed_time');

end

function save_nan_before_exit(outputdatafile,elapsed_time)
dBx=nan;
dBy=nan;
dpx=nan;
dpy=nan;
dBx2=nan;
dBy2=nan;
dpx2=nan;
dpy2=nan;
dBx1=nan;
dBy1=nan;
dpx1=nan;
dpy1=nan;

isorbit=nan;
save(outputdatafile,'dBx','dBy','dpx','dpy','dBx2','dBy2','dpx2','dpy2','dBx1','dBy1','dpx1','dpy1','isorbit','elapsed_time');

end

