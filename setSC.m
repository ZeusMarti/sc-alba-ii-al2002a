%% set SC folder
% oblanco
% 2023oct16

if isempty(getenv('SCROOT'))
    scZeus='/home/zeus/ZeusApps/SimulatedComisioning/SC';
    fprintf( ...
        strcat( ...
        'SCROOT environment variable not configured,', ...
        ' setting default.\n', ...
        scZeus))
    scroot = scZeus;
else
    scroot = getenv('SCROOT');
end
fprintf(strcat('SC configured at :',scroot,'\n'));
addpath(fullfile(scroot));

