function sba_ssh

global FAMLIST THERING GLOBVAL;
E_0 = .51099891e6;
GLOBVAL.E0 = 3e9-E_0;
GLOBVAL.LatticeFile = '6ba_dsh_7_1000_11_4990';
FAMLIST = cell(0);
THERING = cell(0);

QF = sbend('QF', 0.2, -0.004439317473605967, -0.004439317473605967/2, -0.004439317473605967/2, 7.008108728881604, 'BndMPoleSymplectic4Pass');
QD=sbend('QD', 0.68, 0.08483285731205548, 0.08483285731205548/2, 0.08483285731205548/2, -2.6155266314332812, 'BndMPoleSymplectic4Pass');
Q1 = quadrupole('Q3', 0.12, -7.851296956648392,'StrMPoleSymplectic4Pass');
Q2 = quadrupole('Q2', 0.35, 8.935356864719754,'StrMPoleSymplectic4Pass');
Q3 = quadrupole('Q1', 0.22, -9.490273260422695,'StrMPoleSymplectic4Pass');
%IQ1 = quadrupole('IQ3', 0.2, 4.46655255227258,'StrMPoleSymplectic4Pass');
%IQ2 = quadrupole('IQ2', 0.35, -6.955945758754166,'StrMPoleSymplectic4Pass');
%IQ3 = quadrupole('IQ1', 0.2, 8.965164698397658,'StrMPoleSymplectic4Pass');
QFS = sbend('QFS', 0.2, -0.008975477115891473, -0.008975477115891473/2, -0.008975477115891473/2, 7.0, 'BndMPoleSymplectic4Pass');
QDS=sbend('QDS', 0.46, 0.053204401036511466, 0.053204401036511466/2, 0.053204401036511466/2, -0.48746472891837506, 'BndMPoleSymplectic4Pass');



D15CM = drift('D15CM', 0.15, 'DriftPass');
D10CM = drift('D10CM', 0.1, 'DriftPass');
D7CM = drift('D7CM', 0.07, 'DriftPass');
D4CM = drift('D4CM', 0.04, 'DriftPass');
SV0 = drift('SV0', 0.16, 'DriftPass');

D15CM_half = drift('D15CM', 0.15/2, 'DriftPass');
D10CM_half = drift('D10CM', 0.1/2, 'DriftPass');
D5CM_half = drift('D10CM', 0.05/2, 'DriftPass');

BPM=marker('BPM','IdentityPass');

COR = multipole('COR', 0.05, [0 0 0],[0 0 0],'StrMPoleSymplectic4Pass');
FAMLIST{COR}.ElemData.MaxOrder=2;
D15CM_BPM_COR=[D10CM_half COR BPM D10CM_half];
D10CM_BPM_COR=[D5CM_half COR BPM D5CM_half];
D10CM_BPM=[D10CM_half BPM D10CM_half];

SH1 = sextupole('SH1', 0.32, 229.68511813062358/2,'StrMPoleSymplectic4Pass');
SH2 = sextupole('SH2', 0.32, 452.6819853874367/2,'StrMPoleSymplectic4Pass');
SH3 = sextupole('SH3', 0.32, 579.8507887515261/2,'StrMPoleSymplectic4Pass');
SH4 = sextupole('SH4', 0.32, 592.2529020490927/2,'StrMPoleSymplectic4Pass');
SH5 = sextupole('SH5', 0.32, 677.3650055554481/2,'StrMPoleSymplectic4Pass');


SV1 = sextupole('SV1', 0.16, -935.393619991066/2,'StrMPoleSymplectic4Pass');
SV2 = sextupole('SV2', 0.16, -777.8085106701377/2,'StrMPoleSymplectic4Pass');
SV3 = sextupole('SV3', 0.16, -933.4887717052077/2,'StrMPoleSymplectic4Pass');
SV4 = sextupole('SV4', 0.16, -664.8877126638916/2,'StrMPoleSymplectic4Pass');
SV5 = sextupole('SV5', 0.16, -917.2235863499124/2,'StrMPoleSymplectic4Pass');
SV6 = sextupole('SV6', 0.16, -732.7695157281032/2,'StrMPoleSymplectic4Pass');
SV7 = sextupole('SV7', 0.16, -913.7970671071561/2,'StrMPoleSymplectic4Pass');
SV8 = sextupole('SV8', 0.16, -930.3982443571537/2,'StrMPoleSymplectic4Pass');
SV9 = sextupole('SV9', 0.16, -936.1109863134066/2,'StrMPoleSymplectic4Pass');



SS1 = drift('SS', 2.00000925/2, 'DriftPass');
SS = repmat(SS1,[1,2]);
WAIST = marker('WAIST', 'IdentityPass');
SUPP_TO_FOCUS = marker('SUPP_TO_FOCUS', 'IdentityPass');
endq1 = marker('ENDQ1', 'IdentityPass');

Girdstart = marker('GirderStart', 'IdentityPass');
Girdend = marker('GirderEnd', 'IdentityPass');
Secstart = marker('SectionStart', 'IdentityPass');
Secdend = marker('SectionEnd', 'IdentityPass');


APLim = aperture('APLim', [-0.010 0.010 -0.0025 0.0025],'AperturePass');

ARC=[SUPP_TO_FOCUS, QDS, D4CM, SV0, D10CM, QFS, D4CM, SH1, D4CM, QF, Girdend, Girdstart D10CM_BPM,...
    SV1, D4CM, QD, D4CM, SV2, D10CM, QF, D4CM, SH2, D4CM, QF, Girdend, Girdstart D10CM_BPM, SV3,...
    D4CM, QD, D4CM, SV4, D10CM, QF, D4CM, SH3, D4CM, QF, Girdend, Girdstart D10CM_BPM, SV5, D4CM, ...
    QD, D4CM, SV6, D10CM, QF, D4CM, SH4, D4CM, QF, Girdend, Girdstart D10CM_BPM, SV7, D4CM, QD,...
    D4CM, SV8, D10CM, QF, D4CM, SH5, D4CM, QFS, D10CM_BPM, SV9, D4CM, QDS, ...
    SUPP_TO_FOCUS];

ID_TRIPLET=[D15CM_BPM_COR, Q3,  D7CM,  Q2,  D7CM, Q1, D10CM_BPM_COR, SS, WAIST];
ID_TRIPLET_filp=[WAIST, SS, D10CM_BPM_COR, Q1, D7CM, Q2, D7CM, Q3, D15CM_BPM_COR];

UNIT=[Secstart Girdstart ID_TRIPLET_filp,  ARC, ID_TRIPLET Girdend Secdend];

QUADRANT = [ UNIT, UNIT, UNIT, UNIT];
L0 = 2.688002959999981e+02; % to get a true zero cod.
C0 = 299792458; 	% speed of light [m/s]
HarmNumber=448;
CAV	= rfcavity('CAVRF' , 0 , 2.4e+6 , HarmNumber*C0/L0, HarmNumber ,'RFCavityPass');

RING = [APLim,CAV, QUADRANT,endq1, QUADRANT, QUADRANT, QUADRANT];



buildlat(RING);
% Set all magnets to same energy
THERING = setcellstruct(THERING, 'Energy', 1:length(THERING), GLOBVAL.E0);
evalin('caller', 'global THERING FAMLIST GLOBVAL');
if nargout
    varargout{1} = THERING;
end

end
