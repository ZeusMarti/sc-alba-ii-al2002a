function [SC, BPMords, CMords,skords] = register_albaII_1PS_HCMinSH(SC,varargin)

BPMoffsetRMS=500e-6;
skrange=0.1;
if nargin==2
    BPMoffsetRMS=varargin{1};
elseif nargin==3
    BPMoffsetRMS=varargin{1};
    skrange=varargin{2};
end
% Factors for switching off entire error types % % % % % % % % % % % % % % % % % %
injErrorFactor  = 0;
injJitterFactor = 0;
magErrorFactor  = 1;
diagErrorFactor = 1;
RFerrorFactor   = 0;

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Add magnet errors to lattice
magOffset     = 1* [30E-6 30E-6 200E-6]   * magErrorFactor; % Offsets of magnets in x, y and z [m]
magRoll       = 1* [100E-6 100E-6 100E-6] * magErrorFactor; % Roll of magnets around z-, x- and y-axis [rad]
magCal        = 1* 2E-4                   * magErrorFactor; % Relative magnet strength error
BendingError   = 1* 1E-3                   * magErrorFactor; % Relative magnet strength error

SectionOffset = 1* 90E-6 * [1 1 0]       * magErrorFactor; % Offsets of sections in x, y and z [m]
GirderOffset  = 1* 50E-6  * [1 1 0;1 1 0] * magErrorFactor; % Offsets of girders in x, y and z [m]
girderRoll    = 1* [100E-6 0 0]           * magErrorFactor; % Roll of girders around z-, x- and y-axis [rad]


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Add errors to diagnostic devices%
BPMcal          = 1* 5E-2   * [1 1] * diagErrorFactor; % BPM calibration error
BPMoffset       = 1* BPMoffsetRMS * [1 1] * diagErrorFactor; % BPM offset [m]
BPMnoise        = 1* 30E-6  * [1 1] * diagErrorFactor; % BPM noise [m]
BPMnoiseCO      = 1* 1E-6   * [1 1] * diagErrorFactor; % BPM noise for stored beam [m]
BPMroll         = 1* 4E-3           * diagErrorFactor; % BPM roll around z-axis [rad]
CMcal           = 1* 5E-2           * diagErrorFactor; % CM calibration error

CMlimitQuad     = 1* 1.0E-3;
CMlimitSext     = 1* 1.0E-3;
CMlimit     = 1* 0.5E-3;
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Circumfernece error
circError       = 1* 10E-6*RFerrorFactor; % Circumeference error [rel]

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Add errors to RF cavity
RFfrequency     = 1* 1E2  * RFerrorFactor; % RF frequency error [Hz]
RFvoltage       = 1* 1E-3 * RFerrorFactor * SC.IDEALRING{findcells(SC.RING,'Frequency')}.Voltage; % RF voltage error [V]
RftimeLag       = 1* 1/4  * RFerrorFactor * 299792458/SC.IDEALRING{findcells(SC.RING,'Frequency')}.Frequency; % RF phase [m]


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% BPM and OCM status
skewpersector=8;

ords = SCgetOrds(SC.RING,'^BPM$');
nb=numel(ords);
status=true(nb,1);
statusch=true(nb,1);
statuscv=true(nb,1);
    
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get CMs and BPMs used for orbit correction
ords = SCgetOrds(SC.RING,'^COR$|^SH[1,2,3,4,5]$');
hords=ords(statusch);
ords = SCgetOrds(SC.RING,'^COR$|^SV[1,3,5,7,9]$');
vords=ords(statuscv);
CMords  = {hords,vords};
ords = SCgetOrds(SC.RING,'^BPM$');
BPMords=ords(status);



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Register SB: Superbends
%ords = SCgetOrds(SC.RING,'^SB$');
%SC = SCregisterMagnets(SC,ords,...
%	'MagnetOffset',magOffset,...
%	'MagnetRoll',magRoll);

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Register quads
Qords = SCgetOrds(SC.RING,'^Q1$|^Q2$|^Q3$');

SC = SCregisterMagnets(SC,Qords,...
    'CalErrorB',[0 magCal],...
    'MagnetOffset',magOffset,...
    'MagnetRoll',magRoll);


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Register QD/QF/QDS/QFS: bend/Quad (CF)
ords = SCgetOrds(SC.RING,'^QD$|^QDS$|^QF$|^QFS$');
SC = SCregisterMagnets(SC,ords,...
    'CF',1,...
    'BendingAngle',BendingError,...
    'CalErrorB',[0 magCal],...
    'MagnetOffset',magOffset,...
    'MagnetRoll',magRoll);


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Register VCM/HCM/Skew/Sexts
Sords = SCgetOrds(SC.RING,'^SH\d$|^SV[1,2,3,4,5,6,7,8,9]$');
ccords = SCgetOrds(SC.RING,'^COR$');
skords = SCgetOrds(SC.RING,'^SH[2,4]$|^SV[1,3,5,7,9]$');
    

shords=intersect(hords,Sords);
svords=intersect(vords,Sords);
ssords=setdiff(Sords,svords);
ssords=setdiff(ssords,shords);
ssords=setdiff(ssords,skords);

SC = SCregisterMagnets(SC,ccords,...
    'HCM',CMlimit,...
    'VCM',CMlimit,...
    'CalErrorA',[CMcal 0 0],...
    'CalErrorB',[CMcal 0 0],...
    'MagnetOffset',magOffset,...
    'MagnetRoll',magRoll);

SC = SCregisterMagnets(SC,ssords,...
    'CalErrorB',[0 0 magCal],...
    'MagnetOffset',magOffset,...
    'MagnetRoll',magRoll);
SC = SCregisterMagnets(SC,shords,...
    'HCM',CMlimitSext,...
    'CalErrorB',[CMcal 0 magCal],...
    'MagnetOffset',magOffset,...
    'MagnetRoll',magRoll);
SC = SCregisterMagnets(SC,svords,...
    'VCM',CMlimitSext,...
    'CalErrorB',[0 0      magCal],...
    'CalErrorA',[CMcal 0      0],...
    'MagnetOffset',magOffset,...
    'MagnetRoll',magRoll);
SC = SCregisterMagnets(SC,skords,...
    'SkewQuad',skrange,...
    'CalErrorB',[CMcal 0      magCal],...
    'CalErrorA',[CMcal magCal 0     ],...
    'MagnetOffset',magOffset,...
    'MagnetRoll',magRoll);


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Register BPMs
SC = SCregisterBPMs(SC,BPMords,...
    'CalError',BPMcal,...
    'Offset',BPMoffset,...
    'Roll',BPMroll,...
    'Noise',BPMnoise,...
    'NoiseCO',BPMnoiseCO);


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Register cavities
CAVords = findcells(SC.RING,'Frequency');
SC = SCregisterCAVs(SC,CAVords,...
    'FrequencyOffset',RFfrequency,...
    'VoltageOffset',RFvoltage,...
    'TimeLagOffset',RftimeLag);


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Register girders

girderOrds = [SCgetOrds(SC.RING,'GirderStart');SCgetOrds(SC.RING,'GirderEnd')];
SC = SCregisterSupport(SC,...
    'Girder',girderOrds,...
    'Offset',GirderOffset,... % x, y and z, [m]
    'Roll',girderRoll);      % az, ax and ay, [rad]


% Register sections
sectOrds = [SCgetOrds(SC.RING,'SectionStart');SCgetOrds(SC.RING,'SectionEnd')];
SC = SCregisterSupport(SC,...
    'Section',sectOrds,...
    'Offset',SectionOffset); % x, y and z, [m]


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Switch on cavity and radiation
SC.RING = SCcronoff(SC.RING,'cavityon','radiationon');

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define initial bunch emittance
% assume booster is in FC:
SC.INJ.beamSize = diag([250E-6, 100E-6, 250E-6, 100E-6, 1E-3, 1E-4].^2);


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define random injection errors
SC.SIG.randomInjectionZ(1) = injJitterFactor * 100E-6;
SC.SIG.randomInjectionZ(2) = injJitterFactor * 10E-6;
SC.SIG.randomInjectionZ(3) = injJitterFactor * 100E-6;
SC.SIG.randomInjectionZ(4) = injJitterFactor * 10E-6;
SC.SIG.randomInjectionZ(5) = injJitterFactor * 1E-4;
SC.SIG.randomInjectionZ(6) = injJitterFactor * 0.1/360 * 299792458/SC.IDEALRING{SC.ORD.Cavity(1)}.Frequency;

% Define systematic injection errors
SC.SIG.staticInjectionZ(1) = injErrorFactor * 1000E-6;
SC.SIG.staticInjectionZ(2) = injErrorFactor * 100E-6;
SC.SIG.staticInjectionZ(3) = injErrorFactor * 1000E-6;
SC.SIG.staticInjectionZ(4) = injErrorFactor * 100E-6;
SC.SIG.staticInjectionZ(5) = injErrorFactor * 1E-3;
SC.SIG.staticInjectionZ(6) = injErrorFactor * 1E-3;


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Circumference uncertainty
SC.SIG.Circumference = circError;


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Percentage of partcles which can be lost while still getting BPM reading
SC.INJ.beamLostAt    = 0.6;

end
