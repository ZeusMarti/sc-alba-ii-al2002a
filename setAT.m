%% set accelerator toolbox
% oblanco
% 2023oct16

if isempty(getenv('ATROOT'))
    fprintf('This session does not define AT\n');
else
    atroot = getenv('ATROOT');
end
addpath(fullfile(atroot));
addpath(fullfile(atroot,'atmat'));
atpath;

