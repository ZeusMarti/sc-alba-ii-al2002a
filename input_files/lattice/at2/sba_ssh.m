function sba_ssh

global FAMLIST THERING GLOBVAL;
E_0 = .51099891e6;
GLOBVAL.E0 = 3e9-E_0;
GLOBVAL.LatticeFile = '6ba_dsh_7_1000_11_4990';
FAMLIST = cell(0);
THERING = cell(0);

QF = atsbend('QF', 0.2,  -0.004439317473605967, 7.008108728881604,  'BndMPoleSymplectic4Pass','EntranceAngle',-0.004439317473605967/2,'ExitAngle',-0.004439317473605967/2);
QD = atsbend('QD', 0.68,  0.08483285731205548, -2.6155266314332812, 'BndMPoleSymplectic4Pass','EntranceAngle', 0.08483285731205548/2, 'ExitAngle', 0.08483285731205548/2);
Q1 = atquadrupole('Q3', 0.12, -7.851296956648392,'StrMPoleSymplectic4Pass');
Q2 = atquadrupole('Q2', 0.35, 8.935356864719754,'StrMPoleSymplectic4Pass');
Q3 = atquadrupole('Q1', 0.22, -9.490273260422695,'StrMPoleSymplectic4Pass');
%IQ1 = quadrupole('IQ3', 0.2, 4.46655255227258,'StrMPoleSymplectic4Pass');
%IQ2 = quadrupole('IQ2', 0.35, -6.955945758754166,'StrMPoleSymplectic4Pass');
%IQ3 = quadrupole('IQ1', 0.2, 8.965164698397658,'StrMPoleSymplectic4Pass');
QFS = atsbend('QFS', 0.2, -0.008975477115891473, 7.0, 'BndMPoleSymplectic4Pass','EntranceAngle',-0.008975477115891473/2,'ExitAngle',-0.008975477115891473/2);
QDS = atsbend('QDS', 0.46, 0.053204401036511466, -0.48746472891837506, 'BndMPoleSymplectic4Pass','EntranceAngle', 0.053204401036511466/2,'ExitAngle',0.053204401036511466/2);



D15CM = atdrift('D15CM', 0.15, 'DriftPass');
D10CM = atdrift('D10CM', 0.1, 'DriftPass');
D7CM = atdrift('D7CM', 0.07, 'DriftPass');
D4CM = atdrift('D4CM', 0.04, 'DriftPass');
SV0 = atdrift('SV0', 0.16, 'DriftPass');

D15CM_half = atdrift('D15CM', 0.15/2, 'DriftPass');
D10CM_half = atdrift('D10CM', 0.1/2, 'DriftPass');
D5CM_half = atdrift('D10CM', 0.05/2, 'DriftPass');

BPM = atmarker('BPM','IdentityPass');

COR = atmultipole('COR', 0.05, [0 0 0],[0 0 0],'StrMPoleSymplectic4Pass');
% FAMLIST{COR}.ElemData.MaxOrder=2;
COR.MaxOrder=2;
D15CM_BPM_COR={D10CM_half COR BPM D10CM_half};
D10CM_BPM_COR = {D5CM_half COR BPM D5CM_half};
D10CM_BPM = {D10CM_half BPM D10CM_half};

SH1 = atsextupole('SH1', 0.32, 229.68511813062358/2,'StrMPoleSymplectic4Pass');
SH2 = atsextupole('SH2', 0.32, 452.6819853874367/2,'StrMPoleSymplectic4Pass');
SH3 = atsextupole('SH3', 0.32, 579.8507887515261/2,'StrMPoleSymplectic4Pass');
SH4 = atsextupole('SH4', 0.32, 592.2529020490927/2,'StrMPoleSymplectic4Pass');
SH5 = atsextupole('SH5', 0.32, 677.3650055554481/2,'StrMPoleSymplectic4Pass');


SV1 = atsextupole('SV1', 0.16, -935.393619991066/2,'StrMPoleSymplectic4Pass');
SV2 = atsextupole('SV2', 0.16, -777.8085106701377/2,'StrMPoleSymplectic4Pass');
SV3 = atsextupole('SV3', 0.16, -933.4887717052077/2,'StrMPoleSymplectic4Pass');
SV4 = atsextupole('SV4', 0.16, -664.8877126638916/2,'StrMPoleSymplectic4Pass');
SV5 = atsextupole('SV5', 0.16, -917.2235863499124/2,'StrMPoleSymplectic4Pass');
SV6 = atsextupole('SV6', 0.16, -732.7695157281032/2,'StrMPoleSymplectic4Pass');
SV7 = atsextupole('SV7', 0.16, -913.7970671071561/2,'StrMPoleSymplectic4Pass');
SV8 = atsextupole('SV8', 0.16, -930.3982443571537/2,'StrMPoleSymplectic4Pass');
SV9 = atsextupole('SV9', 0.16, -936.1109863134066/2,'StrMPoleSymplectic4Pass');



SS1 = atdrift('SS', 2.00000925/2, 'DriftPass');
SS = {SS1 SS1};
WAIST = atmarker('WAIST', 'IdentityPass');
SUPP_TO_FOCUS = atmarker('SUPP_TO_FOCUS', 'IdentityPass');
endq1 = atmarker('ENDQ1', 'IdentityPass');

Girdstart = atmarker('GirderStart', 'IdentityPass');
Girdend   = atmarker('GirderEnd', 'IdentityPass');
Secstart  = atmarker('SectionStart', 'IdentityPass');
Secdend   = atmarker('SectionEnd', 'IdentityPass');


APLim = ataperture('APLim', [-0.010 0.010 -0.0025 0.0025],'AperturePass');

ARC=[{SUPP_TO_FOCUS, QDS, D4CM, SV0, D10CM, QFS, D4CM, SH1, D4CM, QF, Girdend, Girdstart} D10CM_BPM ...
    {SV1, D4CM, QD, D4CM, SV2, D10CM, QF, D4CM, SH2, D4CM, QF, Girdend, Girdstart} D10CM_BPM, {SV3,...
    D4CM, QD, D4CM, SV4, D10CM, QF, D4CM, SH3, D4CM, QF, Girdend, Girdstart} D10CM_BPM, {SV5, D4CM, ...
    QD, D4CM, SV6, D10CM, QF, D4CM, SH4, D4CM, QF, Girdend, Girdstart} D10CM_BPM, {SV7, D4CM, QD,...
    D4CM, SV8, D10CM, QF, D4CM, SH5, D4CM, QFS}, D10CM_BPM, {SV9, D4CM, QDS, ...
    SUPP_TO_FOCUS}];

ID_TRIPLET = [D15CM_BPM_COR, {Q3},  {D7CM},  {Q2},  {D7CM}, {Q1}, D10CM_BPM_COR, SS, {WAIST}];
ID_TRIPLET_filp = [{WAIST}, SS, D10CM_BPM_COR, {Q1}, {D7CM}, {Q2}, {D7CM}, {Q3}, D15CM_BPM_COR];

UNIT = [{Secstart} {Girdstart} ID_TRIPLET_filp,  ARC, ID_TRIPLET {Girdend} {Secdend}];

QUADRANT = [ UNIT, UNIT, UNIT, UNIT ];
L0 = 2.688002959999981e+02; % to get a true zero cod.
C0 = 299792458; 	% speed of light [m/s]
HarmNumber=448;
CAV	= atrfcavity('CAVRF' , 0 , 2.4e+6 , HarmNumber*C0/L0, HarmNumber ,'RFCavityPass');

RP = atringparam('ALBA-II',3e9);
RING = [{RP} {APLim} {CAV} QUADRANT {endq1} QUADRANT QUADRANT QUADRANT ]';
THERING = RING;


% buildlat(RING);
% Set all magnets to same energy
% THERING = setcellstruct(THERING, 'Energy', 1:length(THERING), GLOBVAL.E0);
THERING = atsetenergy(THERING,GLOBVAL.E0);
evalin('caller', 'global THERING FAMLIST GLOBVAL');
if nargout
    varargout{1} = THERING;
end

end
