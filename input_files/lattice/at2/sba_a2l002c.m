function THERING=sba_a2l002c

global FAMLIST THERING GLOBVAL;
E_0 = .51099891e6;
GLOBVAL.E0 = 3e9-E_0;
GLOBVAL.LatticeFile = 'sba_a2l002c';
FAMLIST = cell(0);
THERING={};

QF = atsbend('QF', 0.2, -0.004439317474, 7.006890642, 'BndMPoleSymplectic4Pass','EntranceAngle', -0.004439317474/2,'ExitAngle', -0.004439317474/2);
QD=atsbend('QD', 0.68, 0.08493894331, -2.633580648, 'BndMPoleSymplectic4Pass','EntranceAngle', 0.08493894331/2,'ExitAngle', 0.08493894331/2);

IQF = atsbend('QF', 0.2, -0.004539269579, 7.181172876, 'BndMPoleSymplectic4Pass','EntranceAngle', -0.004539269579/2,'ExitAngle', -0.004539269579/2);
IQD=atsbend('QD', 0.68, 0.08469973525, -2.580585448, 'BndMPoleSymplectic4Pass','EntranceAngle', 0.08469973525/2,'ExitAngle', 0.08469973525/2);

QFS = atsbend('QFS', 0.2, -0.008975477116, 7.0, 'BndMPoleSymplectic4Pass','EntranceAngle', -0.008975477116/2,'ExitAngle', -0.008975477116/2);
QDS=atsbend('QDS', 0.46, 0.05320440104, -0.4874647289, 'BndMPoleSymplectic4Pass','EntranceAngle', 0.05320440104/2,'ExitAngle', 0.05320440104/2);

IQFS = atsbend('QFS', 0.2, -0.008755728943, 7.0, 'BndMPoleSymplectic4Pass','EntranceAngle', -0.008755728943/2,'ExitAngle', -0.008755728943/2);
IQDS=atsbend('QDS', 0.46, 0.0505208898, -0.4874647289, 'BndMPoleSymplectic4Pass','EntranceAngle', 0.0505208898/2,'ExitAngle', 0.0505208898/2);

Q1 = atquadrupole('Q3', 0.12, -7.851296957,'StrMPoleSymplectic4Pass');
Q2 = atquadrupole('Q2', 0.35, 8.935356865,'StrMPoleSymplectic4Pass');
Q3 = atquadrupole('Q1', 0.22, -9.49027326,'StrMPoleSymplectic4Pass');

IQ1 = atquadrupole('Q3', 0.22, 5.530582095,'StrMPoleSymplectic4Pass');
IQ2 = atquadrupole('Q2', 0.22, -5.794584305,'StrMPoleSymplectic4Pass');

SH1 = atsextupole('SH1', 0.24, 394.3407421707196/2,'StrMPoleSymplectic4Pass');
SH2 = atsextupole('SH2', 0.24, 636.860421801439/2,'StrMPoleSymplectic4Pass');
SH3 = atsextupole('SH3', 0.24, 863.2869236173086/2,'StrMPoleSymplectic4Pass');
SH4 = atsextupole('SH4', 0.24, 697.2231218275812/2,'StrMPoleSymplectic4Pass');
SH5 = atsextupole('SH5', 0.24, 785.9385568470726/2,'StrMPoleSymplectic4Pass');

SV0 = atdrift('DrSV0', 0.16, 'DriftPass');
SV1 = atsextupole('SV1', 0.16, -949.9991265741166/2,'StrMPoleSymplectic4Pass');
SV2 = atsextupole('SV2', 0.16, -874.1671976841343/2,'StrMPoleSymplectic4Pass');
SV3 = atsextupole('SV3', 0.16, -949.9991194028303/2,'StrMPoleSymplectic4Pass');
SV4 = atsextupole('SV4', 0.16, -794.4642843168311/2,'StrMPoleSymplectic4Pass');
SV5 = atsextupole('SV5', 0.16, -949.9982191639122/2,'StrMPoleSymplectic4Pass');
SV6 = atsextupole('SV6', 0.16, -680.1810324952728/2,'StrMPoleSymplectic4Pass');
SV7 = atsextupole('SV7', 0.16, -698.061849783137/2,'StrMPoleSymplectic4Pass');
SV8 = atsextupole('SV8', 0.16, -949.9991234666337/2,'StrMPoleSymplectic4Pass');
SV9 = atsextupole('SV9', 0.16, -949.971839538188/2,'StrMPoleSymplectic4Pass');

ISH1A = atsextupole('SH1', 0.24, 663.8057743161271/2,'StrMPoleSymplectic4Pass');
ISH2A = atsextupole('SH2', 0.24, 877.0656106141978/2,'StrMPoleSymplectic4Pass');
ISH3A = atsextupole('SH3', 0.24, 934.2983928611224/2,'StrMPoleSymplectic4Pass');
ISH4A = atsextupole('SH4', 0.24, 653.4720893649445/2,'StrMPoleSymplectic4Pass');
ISH5A = atsextupole('SH5', 0.24, 510.2314596785512/2,'StrMPoleSymplectic4Pass');

ISV0A = atsextupole('SV0', 0.16, -610.0711890195723/2,'StrMPoleSymplectic4Pass');
ISV1A = atsextupole('SV1', 0.16, -707.1969343542678/2,'StrMPoleSymplectic4Pass');
ISV2A = atsextupole('SV2', 0.16, -756.5338419874134/2,'StrMPoleSymplectic4Pass');
ISV3A = atsextupole('SV3', 0.16, -949.6992080811275/2,'StrMPoleSymplectic4Pass');
ISV4A = atsextupole('SV4', 0.16, -797.4061894061289/2,'StrMPoleSymplectic4Pass');

ISH1B = atsextupole('SH1', 0.24, 260.1822565415102/2,'StrMPoleSymplectic4Pass');
ISH2B = atsextupole('SH2', 0.24, 622.2884175355326/2,'StrMPoleSymplectic4Pass');
ISH3B = atsextupole('SH3', 0.24, 837.5530776213664/2,'StrMPoleSymplectic4Pass');
ISH4B = atsextupole('SH4', 0.24, 889.3504119909878/2,'StrMPoleSymplectic4Pass');
ISH5B = atsextupole('SH5', 0.24, 536.7465745637181/2,'StrMPoleSymplectic4Pass');

ISV5B = atsextupole('SV5', 0.16, -796.2236298206008/2,'StrMPoleSymplectic4Pass');
ISV6B = atsextupole('SV6', 0.16, -890.2763789375606/2,'StrMPoleSymplectic4Pass');
ISV7B = atsextupole('SV7', 0.16, -878.1545948576853/2,'StrMPoleSymplectic4Pass');
ISV8B = atsextupole('SV8', 0.16, -625.4480332725348/2,'StrMPoleSymplectic4Pass');
ISV9B = atsextupole('SV9', 0.16, -534.7747940006951/2,'StrMPoleSymplectic4Pass');


D31CM = atdrift('D31CM', 0.31, 'DriftPass');
D25CM = atdrift('D25CM', 0.25, 'DriftPass');
D15CM = atdrift('D15CM', 0.15, 'DriftPass');
D12CM = atdrift('D12CM', 0.12, 'DriftPass');
D10CM = atdrift('D10CM', 0.1, 'DriftPass');
D8CM = atdrift('D8CM', 0.08, 'DriftPass');
D7CM = atdrift('D7CM', 0.07, 'DriftPass');
D5CM = atdrift('D5CM', 0.05, 'DriftPass');
D4CM = atdrift('D4CM', 0.04, 'DriftPass');



D15CM_half = atdrift('D15CM', 0.15/2, 'DriftPass');
D10CM_half = atdrift('D10CM', 0.1/2, 'DriftPass');
D5CM_half = atdrift('D10CM', 0.05/2, 'DriftPass');

BPM=atmonitor('BPM');

COR = atmultipole('COR', 0.05, [0 0 0],[0 0 0],'StrMPoleSymplectic4Pass');
COR.MaxOrder=2;
D15CM_BPM_COR={D10CM_half COR BPM D10CM_half};
D10CM_BPM_COR={D5CM_half COR BPM D5CM_half};
D10CM_BPM={D10CM_half BPM D10CM_half};

SS_half = atdrift('SS', 2.00000925/2, 'DriftPass');
SS = {SS_half,SS_half};
WAIST = atmarker('WAIST', 'IdentityPass');
SUPP_TO_FOCUS = atmarker('SUPP_TO_FOCUS', 'IdentityPass');
SUPP_TO_ID = atmarker('SUPP_TO_ID', 'IdentityPass');
SUPP_TO_INJ= atmarker('SUPP_TO_INJ', 'IdentityPass');
endq1 = atmarker('ENDQ1', 'IdentityPass');

Girdstart = atmarker('GirderStart', 'IdentityPass');
Girdend = atmarker('GirderEnd', 'IdentityPass');
Secstart = atmarker('SectionStart', 'IdentityPass');
Secdend = atmarker('SectionEnd', 'IdentityPass');


APLim = ataperture('APLim', [-0.010 0.010 -0.0025 0.0025],'AperturePass');

ARC={SUPP_TO_ID, QDS, D4CM, SV0, D10CM, QFS, D8CM, SH1, D8CM, QF,Girdend, Girdstart,...
    D10CM_BPM{:}, SV1, D4CM, QD, D4CM, SV2, D10CM, QF, D8CM, SH2, D8CM, QF,Girdend, Girdstart,...
    D10CM_BPM{:}, SV3, D4CM, QD, D4CM, SV4, D10CM, QF, D8CM, SH3, D8CM, QF,Girdend, Girdstart,...
    D10CM_BPM{:}, SV5, D4CM, QD, D4CM, SV6, D10CM, QF, D8CM, SH4, D8CM, QF,Girdend, Girdstart,...
    D10CM_BPM{:}, SV7, D4CM, QD, D4CM, SV8, D10CM, QF, D8CM, SH5, D8CM, QFS,...
    D10CM_BPM{:}, SV9, D4CM, QDS, SUPP_TO_ID};

IARC1={SUPP_TO_INJ, QDS, D4CM, ISV0A, D10CM, QFS, D8CM, ISH1A, D8CM, IQF,Girdend, Girdstart,...
    D10CM_BPM{:}, ISV1A, D4CM, IQD, D4CM, ISV2A, D10CM, IQF, D8CM, ISH2A, D8CM, IQF, Girdend, Girdstart,...
    D10CM_BPM{:}, ISV3A, D4CM, IQD, D4CM, ISV4A, D10CM, IQF, D8CM, ISH3A, D8CM, IQF,Girdend, Girdstart, ...
    D10CM_BPM{:}, SV5, D4CM, IQD, D4CM, SV6, D10CM, IQF, D8CM, ISH4A, D8CM, IQF,Girdend, Girdstart, ...
    D10CM_BPM{:}, SV7, D4CM, IQD, D4CM, SV8, D10CM, IQF, D8CM, ISH5A, D8CM, IQFS, ...
    D10CM_BPM{:}, SV9, D4CM, IQDS, SUPP_TO_ID};

IARC2={IQDS, D4CM, SV0, D10CM, IQFS, D8CM, ISH1B, D8CM, IQF,Girdend, Girdstart,...
    D10CM_BPM{:}, SV1, D4CM, IQD, D4CM, SV2, D10CM, IQF, D8CM, ISH2B, D8CM, IQF,Girdend, Girdstart,...
    D10CM_BPM{:}, SV3, D4CM, IQD, D4CM, SV4, D10CM, IQF, D8CM, ISH3B, D8CM, IQF,Girdend, Girdstart,...
    D10CM_BPM{:}, ISV5B, D4CM, IQD, D4CM, ISV6B, D10CM, IQF, D8CM, ISH4B, D8CM, IQF,Girdend, Girdstart,...
    D10CM_BPM{:}, ISV7B, D4CM, IQD, D4CM, ISV8B, D10CM, IQF, D8CM, ISH5B, D8CM, QFS, ...
    D10CM_BPM{:}, ISV9B, D4CM, QDS};



ID_TRIPLET={D15CM_BPM_COR{:}, Q3,  D7CM, Q2,  D7CM, Q1, D10CM_BPM_COR{:}, SS{:}, WAIST};
INJ_DOUBLET={D15CM_BPM_COR{:}, IQ2, D7CM, D25CM, D7CM, IQ1, D10CM_BPM_COR{:}, SS{:}, WAIST};
ID_TRIPLET_flip={WAIST,SS{:},D10CM_BPM_COR{:},Q1,D7CM,Q2,D7CM,Q3,D15CM_BPM_COR{:}};
INJ_DOUBLET_flip={WAIST,SS{:},D10CM_BPM_COR{:},IQ1,D7CM,D25CM,D7CM,IQ2,D15CM_BPM_COR{:}};


UNIT={Secstart Girdstart ID_TRIPLET_flip{:} ARC{:} ID_TRIPLET{:} Girdend Secdend};
INJECTION1={Secstart Girdstart INJ_DOUBLET_flip{:} IARC1{:} ID_TRIPLET{:} Girdend Secdend};
INJECTION2={Secstart Girdstart ID_TRIPLET_flip{:} IARC2{:} INJ_DOUBLET{:} Girdend Secdend};
UNIT14=[UNIT UNIT UNIT UNIT UNIT UNIT UNIT UNIT UNIT UNIT UNIT UNIT UNIT UNIT];

L0 = 2.688002959999981e+02; % to get a true zero cod.
C0 = 299792458; 	% speed of light [m/s]
HarmNumber=448;
CAV	= atrfcavity('CAVRF' , 0 , 2.4e+6 , HarmNumber*C0/L0, HarmNumber, GLOBVAL.E0,'RFCavityPass');

THERING=[{APLim,CAV} INJECTION1 UNIT14 INJECTION2];
THERING=THERING(:);

%buildlat(RING);
% Set all magnets to same energy
%THERING=atsetenergy(THERING, GLOBVAL.E0);
RP=atringparam('A2l002c',GLOBVAL.E0,1);
THERING=[{RP};THERING];
THERING=atsetenergy(THERING, GLOBVAL.E0);
evalin('caller', 'global THERING FAMLIST GLOBVAL');
if nargout
    varargout{1} = THERING;
end

end
