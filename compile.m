
% add path to matching functions
%addpath('/home/zeus/controlroom/at');
%addpath('/home/zeus/controlroom/at/atphysics');
%addpath('/home/zeus/controlroom/at/lattice');
%addpath('/home/zeus/controlroom/at/simulator/element');
%addpath('/home/zeus/controlroom/at/simulator/track');


% Set at paths


%% use MML shortcut

rmpath /home/zeus/controlroom/applications/xml/geodise;

mcc('-m','run_albaII_sc_HCMinSH.m',...
    '-a', '/home/zeus/controlroom/at2/atintegrators/*.mexa64',...
    '-a', '/home/zeus/ZeusApps/SimulatedComisioning/SC/*.m',...
    '-R','-nojvm','-R','-nodisplay');




