function [pop, evalTime] = EvalEffect(pop, options,varargin)
% Function: [pop, evalTime] = evalGeneration(pop,options, varargin)
% Description: Evaluate Generation objective function.
%
%         LSSSSWC, NWPU
%    Revision: 1.1  Data: 2011-07-25
%    Modified: Z.Marti  Data: 2016-03-22
%*************************************************************************

tStart = tic;

MRTfolder='/mnt/hpcsoftware/MATLAB/MATLAB_Runtime/v90';
N=options.popsize;
routine=options.routine;
parms_file=options.parms_file;
numObj=options.numObj; 
queue=options.queue;
user=options.user;
if isfield(options,'maximtime')
    maximtime=options.maximtime;
else
    maximtime='00:10:00';
end
%% generate input files
st=importdata(parms_file);
Nproc=st.Nproc;
if Nproc~=N
    error('Nproc should be equal to population size!');
end
b=dir('specfile_*.mat');
finproc=numel(b);
if finproc==N
    warning('Resuming previous calculations');
else
    for indproc=1:N
        st.outfilename=fullfile(pwd,['outfile_' num2str(indproc,'%d') '.mat']);
        st.var=pop(indproc).var;
        spfn=['specfile_' num2str(indproc,'%d') '.mat'];
        save(spfn,'-struct','st');
    end
end

%% Send all tasks

AutoCompleteTaskList(user,routine,MRTfolder,N,maximtime,queue);

%% collect tasks

b=dir('outfile*.mat');
finproc=length(b);
if(finproc<N)
    error(sprintf('Only %d/%d tasks were properly executed!',finproc,N));
end

y=zeros(N,numObj);
evalTimes=zeros(N,1);

for iproc=1:N
    load(['outfile_' num2str(iproc,'%d') '.mat'],'dBx','dBy','dpx','dpy',...
        'dBx1','dBy1','dpx1','dpy1','dBx2','dBy2','dpx2','dpy2','isorbit','elapsed_time');
    y(iproc,:)=[dBx dBy dpx dpy isorbit dBx1 dBy1 dpx1 dpy1 dBx2 dBy2 dpx2 dpy2];
    evalTimes(iproc)=elapsed_time;
end

delete('*.stdout');
delete('*.stderr');
delete('*.out');
delete('spec*.mat');
delete('out*.mat');
evalTime = toc(tStart);
fprintf('Elapsed time is: %1.1fs .Parallelization gives %1.0f%% speedup...\n',evalTime,100*sum(evalTimes)/evalTime-100);


%% Save the objective values and constraint violations

for ii=1:N
    pop(ii).obj=y(ii,:);
end

end

function AutoCompleteTaskList(user,routine,MRTfolder,N,maximtime,queue)
Nmaxlocal=GetMaxNodeProcess(N);% leave a node free
isnotdone=true;

list=1:N;
counter=0;
[list2do,isnotdone,~,~]=WhichJobsToDoNext(N,Nmaxlocal,user,list);
while isnotdone
    counter=counter+1;
    if not(isempty(list2do))
        SendTaskList(routine,MRTfolder,list2do,maximtime,queue);
    end
    pause(10);
    [list2do,isnotdone,list_notdone,list_doing]=WhichJobsToDoNext(N,Nmaxlocal,user,list);
    
    delete(['/home/' user '/hs_error_pid*.*']);
    delete(['/home/' user '/matlab_crash*.*']);
    if counter==6
        disp(['waiting for ' num2str(numel(list_doing)) ' tasks, ' num2str(N-numel(list_notdone)) ' out of ' num2str(N) ' tasks completed.']);
        counter=0;
    end
end
end

function [list2do,isnotdone,list_notdone,list_doing]=WhichJobsToDoNext(N,Nmaxlocal,user,list)

list_doing=JobsInProgressList(user);
[isnotdone, list_notdone]=CheckTask(N);
if isempty(list_doing)&& isempty(list_notdone)
    list2do=1:Nmaxlocal;
else
    if numel(list_doing)<Nmaxlocal
        isnotdone_each=false(N,1);
        isdoing_each=false(N,1);
        isnotdone_each(list_notdone)=true;
        isdoing_each(list_doing)=true;
        is2bedone_each=isnotdone_each&not(isdoing_each);
        list2bedone_each=list(is2bedone_each);
        nfree=Nmaxlocal-numel(list_doing);
        if nfree<numel(list2bedone_each)
            list2do=list2bedone_each(1:nfree);
        else
            list2do=list2bedone_each;
        end
    else
        list2do=[];
    end
end
end

function Nmaxlocal=GetMaxNodeProcess(N)
nonodes=true;
while nonodes
    [~, nodes]=system('sinfo -p short --states=idle -o %D');% --nodes=hpcnode015,hpcnode016');
    if numel(nodes)>6
        nonodes=false;
    else
        warning('No nodes abailable...')
        pause(30);
    end
end
nodemax=str2double(nodes(7:end));

if nodemax==1
    Nmax=23;
elseif nodemax==2
    Nmax=46;
elseif nodemax==3
    Nmax=60;
elseif nodemax==4
    Nmax=80;
elseif nodemax==5
    Nmax=90;
elseif nodemax==6
    Nmax=100;
elseif nodemax>6
    Nmax=floor(100*nodemax/6);
end

Nmaxlocal=Nmax-5;
if N<Nmax
    Nmaxlocal=N;
end
end

function list=JobsInProgressList(user)
[status, out_queue]=system(sprintf('squeue -u %s -o %%i',user));

if numel(out_queue)>7&&not(status)
    completelist=out_queue(7:end);
    totalsent=numel(strfind(completelist,'_'));
    text_queue=textscan(completelist,'%d_%d');
    list=text_queue{2}(:)';
    if numel(list)~=totalsent
        while numel(list)~=totalsent
            warning('squeue shows not R nor S jobs...');
            pause(60);
            [status, out_queue]=system(sprintf('squeue -u %s -o %%i',user));
            if numel(out_queue)>7&&not(status)
                completelist=out_queue(7:end);
                totalsent=numel(strfind(completelist,'_'));
                text_queue=textscan(completelist,'%d_%d');
                list=text_queue{2}(:)';
            else
                totalsent=0;
                list=[];
            end
        end
    end
else
    list=[];
end

end

function jobid=SendTaskList(routine,MRTfolder,list0,maximtime,queue)
fid=fopen('job_array.sl','w');
fprintf(fid,'#!/bin/bash\n');
fprintf(fid,'\n');
fprintf(fid,'#SBATCH -n 1 # un core/process \n');
fprintf(fid,'#SBATCH -t %s #temps maxim d-hh:mm:ss\n',maximtime);
fprintf(fid,'#SBATCH -p %s # partition fast,short,medium,long\n',queue);
%fprintf(fid,'#SBATCH --nodelist=hpcnode018 \n');%,hpcnode016 \n');

fprintf(fid,'#SBATCH -J %s\n',routine);% job name!
counting=false;

list=num2str(list0(1));
for ii=2:numel(list0)
    if list0(ii)-list0(ii-1)>1
        if counting
            list=[list '-' num2str(list0(ii-1)) ',' num2str(list0(ii))];
        else
            list=[list ',' num2str(list0(ii))];
        end
        counting=false;
    else
        if ii==numel(list0)
            list=[list '-' num2str(list0(ii))];
        end
        counting=true;
    end
end
fprintf(fid,'#SBATCH --array=%s\n',list);

fprintf(fid,'#SBATCH --get-user-env\n');
fprintf(fid,'#SBATCH --export=all\n');
fprintf(fid,'\n');
fprintf(fid,'LD_LIBRARY_PATH=$LD_LIBRARY_PATH:%s/runtime/glnxa64/\n',MRTfolder);
fprintf(fid,'LD_LIBRARY_PATH=$LD_LIBRARY_PATH:%s/bin/glnxa64/\n',MRTfolder);
fprintf(fid,'LD_LIBRARY_PATH=$LD_LIBRARY_PATH:%s/sys/os/glnxa64/\n',MRTfolder);
fprintf(fid,'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:%s/sys/opengl/lib/glnxa64/\n',MRTfolder);
fprintf(fid,'export MCR_CACHE_VERBOSE=true\n');
fprintf(fid,'export MCR_INHIBIT_CTF_LOCK=1\n');
fprintf(fid,'./%s ./specfile_${SLURM_ARRAY_TASK_ID}.mat\n',routine);
fclose(fid);
% run on cluster
[~, out_batch]=system('sbatch --export=ALL job_array.sl');
text_sbatch=textscan(out_batch,'%s %s %s %d');
jobid=text_sbatch{end};

end

function [isnotdone, list]=CheckTask(N)

b=dir('outfile*.mat');
finproc=length(b);

isnotdone=finproc<N;
list=1:N;
isthere=zeros(numel(b),1);
for ii=1:numel(b)
    name=b(ii).name;
    isthere(ii)=str2num(name(9:(end-4)));
end
list(isthere)=[];

end





