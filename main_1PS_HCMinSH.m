%% Prepare the run
global THERING;
cd /home/zeus/ZeusApps/Studies/SC_albaII_A2L002a/;


nseeds=100;
pop=struct;
BPMoffsetRMS=500*1e-6;
skrange=0.1;
nskr=numel(skrange);
count=0;
for ii=1:nseeds
    for kk=1:nskr
        count=count+1;
        pop(count).var=[ii];
    end
end

Nproc=count; % number of processes

%remove repeatedzero points
numVar=numel(pop(count).var);
var=33;
numObj=13;


rf_correction_method='zeus';
outfilename='out_test.mat';
parms_file='GlobalParameters.mat';
save(parms_file,'Nproc','numObj','numVar','var','outfilename','rf_correction_method',...
    'BPMoffsetRMS','skrange');


%% main execution section


options = struct;                    % create default options structure
options.parms_file=parms_file;
options.maximtime='08:00:00';
options.useParallel='yes';
options.popsize = Nproc;                   % populaion size
options.numObj = numObj;                     % number of objectives
options.numVar = numVar;                    % number of design variables
options.numCons = 0;                 % number of constraints
options.routine = 'run_albaII_sc';       % objective function handle
options.plotInterval = 1;              % large interval for efficiency
options.outputInterval = 1;
options.user='zeus';                    %used to squeue only my tasks
options.queue='medium';

[pop, evalTime] = EvalEffect(pop, options);
ara=datestr(datenum(clock),'mmm_dd_yyyy-HH-MM-SS');
save(['results_' ara '.mat'],'pop','options','nseeds','bpmerr',...
    'rf_correction_method','BPMoffsetRMS');


%% betabeat, Lifetime and Hor DA

label_case='Sep_16_2022-05-53-18';
%label_case='Sep_21_2022-16-22-22';% RF nsteps to 45
%label_case='Oct_04_2022-11-30-53';% SCBBA first implemented, RF nsteps to 45, zeus RF correction
label_case='Oct_04_2022-13-09-10';% minBPMrangeOtherBPM=200e-6, SCBBA Thorsten version, RF nsteps to 45, zeus RF correction
%label_case='Oct_04_2022-18-06-55';% minBPMrangeOtherBPM=150e-6, SCBBA Thorsten version, RF nsteps to 45, zeus RF correction
%label_case='Oct_05_2022-06-05-29';% minBPMrangeOtherBPM=250e-6, SCBBA Thorsten version, RF nsteps to 45, zeus RF correction
label_case='Oct_06_2022-06-05-51';% with CO BBA, minBPMrangeOtherBPM=200e-6, SCBBA Thorsten version, RF nsteps to 45, zeus RF correction
label_case='Oct_07_2022-03-17-47';% with CO BBA(1CM), minBPMrangeOtherBPM=200e-6, SCBBA Thorsten version, RF nsteps to 45, zeus RF correction
label_case='Oct_10_2022-20-24-15';% with CO BBA(1CM,quads only Q1,Q3), minBPMrangeOtherBPM=200e-6, SCBBA Thorsten version, RF nsteps to 45, zeus RF correction
label_case='Oct_25_2022-10-50-54';% LOCO, with CO BBA(1CM,quads only Q1,Q3), minBPMrangeOtherBPM=200e-6, SCBBA Thorsten version, RF nsteps to 45, zeus RF correction
label_case='Dec_07_2022-05-14-13';% LOCO procedure works
load(['results_' label_case '.mat']);

obj=reshape([pop.obj],[numObj,nseeds,nbpme]);
Bx=reshape(squeeze(obj(1,:,:)),[nseeds,nbpme]);
By=reshape(squeeze(obj(2,:,:)),[nseeds,nbpme]);
px=reshape(squeeze(obj(3,:,:)),[nseeds,nbpme]);
py=reshape(squeeze(obj(4,:,:)),[nseeds,nbpme]);
isorbit=reshape(squeeze(obj(5,:,:)),[nseeds,nbpme]);
isorbit(isnan(isorbit))=0;

Bxmean=zeros(nbpme,1);
Bymean=zeros(nbpme,1);
for ii=1:nbpme
    Bxmean(ii)=mean(Bx(not(isnan(Bx(:,ii))),ii));
    Bymean(ii)=mean(By(not(isnan(By(:,ii))),ii));
end

subplot(2,1,1);
hold all;
plot(bpmerr*1e6,100*Bx(1,:),'ro');hold all;plot(bpmerr*1e6,100*By(1,:),'bx');
plot(bpmerr*1e6,100*Bx','ro');hold all;plot(bpmerr*1e6,100*By','bx');
plot(bpmerr*1e6,100*Bxmean,'-r');hold all;plot(bpmerr*1e6,100*Bymean,'-b');
ylim([0 200]);
ylabel('\Delta\beta/\beta [%]');
legend('\beta_x','\beta_y');
legend('Location','NorthWest');
subplot(2,1,2);
hold all;
plot(bpmerr*1e6,(nseeds-sum(isorbit))/nseeds*100);
xlabel('BPM offset error [\mum]');
ylabel('Not a closed orbit [%]');
SetPlotSize(10,10);
print(['Betas_noorbit_' label_case '.png'],'-r500','-dpng');

%% Compare two cases


cases={'Sep_21_2022-16-22-22',...% RF nsteps to 45
    'Oct_04_2022-13-09-10',...%... minBPMrangeOtherBPM=200e-6, SCBBA Thorsten version, RF nsteps to 45, zeus RF correction
'Oct_10_2022-20-24-15',...% with CO BBA(1CM,quads only Q1,Q3), minBPMrangeOtherBPM=200e-6, SCBBA Thorsten version, RF nsteps to 45, zeus RF correction
'Dec_07_2022-05-14-13'};

col={{'-r','-b'},{'--r','--b'},{':r',':b'},{'-r.','-b.'}};
colk={'-k','--k',':k','-k.'};
numObjs=[5 5 5 13];
for kk=1:numel(cases)
    
    
    load(['results_' cases{kk} '.mat']);
    numObj=numObjs(kk);
    obj=reshape([pop.obj],[numObj,nseeds,nbpme]);
    Bx=reshape(squeeze(obj(1,:,:)),[nseeds,nbpme]);
    By=reshape(squeeze(obj(2,:,:)),[nseeds,nbpme]);
    px=reshape(squeeze(obj(3,:,:)),[nseeds,nbpme]);
    py=reshape(squeeze(obj(4,:,:)),[nseeds,nbpme]);
    isorbit=reshape(squeeze(obj(5,:,:)),[nseeds,nbpme]);
    isorbit(isnan(isorbit))=0;
    
    Bxmean=zeros(nbpme,1);
    Bymean=zeros(nbpme,1);
    for ii=1:nbpme
        Bxmean(ii)=mean(Bx(not(isnan(Bx(:,ii))),ii));
        Bymean(ii)=mean(By(not(isnan(By(:,ii))),ii));
    end
    
    subplot(3,1,2);
    semilogy(bpmerr*1e6,100*Bxmean,col{kk}{1});
    box on;hold all
    ylim([1 110]);
    yl2=ylabel('\Delta\beta_x/\beta_x [%]');
    xlim([0 500]);
    subplot(3,1,3);
    semilogy(bpmerr*1e6,100*Bymean,col{kk}{2}); 
    box on;hold all
    ylim([1 110]);
    yl3=ylabel('\Delta\beta_y/\beta_y [%]');
    xlabel('BPM offset error [\mum]');
    xlim([0 500]);
    subplot(3,1,1);
    hold all;
    box on;
    xlim([0 500]);
    coc=(nseeds-sum(isorbit))/nseeds*100;
    if kk==4
        plot(bpmerr*1e6,cocant,colk{kk});
        
    else
        plot(bpmerr*1e6,coc,colk{kk});
    end
    
    yl1=ylabel('Not a closed orbit [%]');
    cocant=coc;
end
legend('FT+RF','FT+RF+TbTBBA','FT+RF+TbTBBA+COTbT','FT+RF+TbTBBA+CO TbT+LOCO')
h=legend('Location','NorthWest');
%legend('boxoff')
SetPlotSize(8,15);
p=get(h,'Position');
p(1)=p(1)+0.25;
p(2)=p(2)+0.09;

set(h,'Position',p);

% p=get(yl1,'Position');
% p(1)=p(1)+0.0001;
% set(yl1,'Position',p);

p=get(yl2,'Position');
p(1)=p(1)+0.0001;
set(yl2,'Position',p);

p=get(yl3,'Position');
p(1)=p(1)+0.0001;
set(yl3,'Position',p);

print(['Compare_Betas_noorbit_' cases{4} '.png'],'-r800','-dpng');

%% run some times

cases_no=[10    20    48    71    75    77    97    99];
for cas=[97 99]
    st=load('GlobalParameters.mat');
    st.var=[cas st.var(2) st.var(3)];
    save('GlobalParameters.mat','-struct','st');
    run_albaII_sc('GlobalParameters.mat');
    sto=load(st.outfilename);
    save(['Case_' int2str(cas) '_' st.outfilename],'-struct','sto');
end

